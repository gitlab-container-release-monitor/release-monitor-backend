# Release Monitor Backend

## How to run
First, get yourself an access token.  
Follor [this](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
guide.  
Select `api` when beeing asked for a scope.

```bash
IMAGE="registry.gitlab.com/gitlab-container-release-monitor/release-monitor-backend:main"
docker run -d \
  -e GITLAB_ACCESS_TOKEN="PASTE-ACCESSTOKEN-HERE" \
  -e MOUNT=https://releasemonitor.julianbuettner.dev/api \
  -e PROJECTS=35226132,35226124 \
  -p 3000:3000 \
  --restart unless-stopped \
  --name release-monitor-backend \
  "$IMAGE"
```

## API Definition
OpenAPI? Never heard about it.

`GET /releases/`
```json
[
  "2022.05",
  "2022.04",
  "2022.03",
]
```

`GET /projects`
```json
[
  {
    "name": "Release Monitor Backend",
    "id": 1234123
  }
]
```

Get Git tags that are supposed to trigger
a build pipeline and therefore a Docker build.  
`GET /project/:projectId/release/{release}/tags`
```json
[
  {
    "tag": "2022.05.1",
    "timestamp": 1649277998
  }
]
```

`GET /project/{project_id}/tag/{tag}/pipeline`
```json
{
  "url": "https://gitlab.com/myname/pipeline/123456",
  "timestamp": 1649277998,
  "state": "pending"
}
```

Always the last pipeline run counts.

List of possible pipeline states:
```yaml
- not created   # no pipeline triggered for git tag
- pending       # not picked up by a runner
- running
- failed
- successful
- unknown       # reserved
```

`GET /project/{project_id}/tag/{tag}/container`
```json
{
  "timestamp": 1649277998,
  "bytes": 12341234,
  "short_digest": "12hexabc12",
  "aliases": [
    "latest",
    "2022.03"
  ]
}
```
