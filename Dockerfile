FROM node:17-buster-slim

WORKDIR /usr/src/app

COPY package*.json ./
COPY tsconfig.json ./

RUN npm install

COPY ./src ./src

RUN npm run build

EXPOSE 5000

ENV HOST=0.0.0.0
ENV BACKEND_URL="https://localhost:3000"

# Rebuild (set env variable) ans start
CMD [ "npm", "run", "start" ]
