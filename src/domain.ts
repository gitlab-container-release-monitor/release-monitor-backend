import { parseTag, Tag as ContainerTag} from "./tagging";
import { GitlabState} from "./gitlabstate";
import {
  Project,
  GitTag,
  Pipeline,
  PipelineStatus,
  Container,
} from "./dto";

export function tagToGroupReleaseString(tag: string): string | null {
  const pattern = /v?(?<major>[0-9]+).(?<minor>[0-9]+)\.[0-9]+.*/;
  const match = pattern.exec(tag);
  if (match === null) {
    return null;
  }
  const res = `${match.groups?.major}.${match.groups?.minor}`;
  return res;
}


export async function getProjects(): Promise<Array<Project>> {
  const gitlab = GitlabState.getInstance();
  let result = [];
  for (let project of gitlab.projects) {
    result.push(new Project(
      project.id,
      project.name,
      project.description,
      project.url,
      project.avatarUrl,
    ));
  }
  return result;
}

export async function getReleases(): Promise<Array<string>> {
  const gitlab = GitlabState.getInstance();
  let groupReleases: Set<string> = new Set();
  for (let project of gitlab.projects) {
    for (let tag of project.tags) {
      groupReleases.add(tagToGroupReleaseString(tag.name));
    }
  }
  return Array.from(groupReleases);
}

export async function getTagsOfProjectOfRelease(
  projectId: number,
  release: string,
): Promise<Array<GitTag>> {
  const gitlab = GitlabState.getInstance();
  const project = gitlab.getProject(projectId);
  return project.tags.filter((x) => release === tagToGroupReleaseString(x.name));
}

export async function getLatestOfProjectOfRelease(
  projectId: number,
  release: string,
): Promise<{highestContainer: Container | null, highestGitTag: GitTag | null}> {
  const gitlab = GitlabState.getInstance();
  const project = gitlab.getProject(projectId);
  
  function getHighestTag(tagArray: Array<string>) {
    const tags = tagArray.map((t) => parseTag(t))
      .filter((t) => t !== null)
      .filter((t) => t.isComplete());
    if (tags.length === 0) {
      return null;
    }
    let highestTag = tags[0];
    for (let tag of tags) {
      if (tag.greaterThan(highestTag)) {
        highestTag = tag;
      }
    }
    return highestTag;
  }

  const highestContainerTag = getHighestTag(
    project.containers
      .map((x) => x.tag)
      .filter((t) => tagToGroupReleaseString(t) === release)
  )?.original;
  const highestGitTag = getHighestTag(
    project.tags
      .map((t) => t.name)
      .filter((t) => tagToGroupReleaseString(t) === release)
  )?.original;

  const highestContainer = project.getContainer(highestContainerTag);
  const highestTag = project.getTag(highestGitTag);

  let res = {
    highestContainer: null,
    highestGitTag: null,
  };
  if (highestContainer) {
    res.highestContainer = Container.fromGitlab(highestContainer, []);
  }
  if (highestTag) {
    res.highestGitTag = GitTag.fromGitlab(highestTag);
  }

  return res;
}

export async function getPipelineOfTag(
  projectId: number,
  tag: string,
): Promise<Pipeline> {
  const gitlab = GitlabState.getInstance();
  const project = gitlab.getProject(projectId);
  for (let pipeline of project.pipelines) {
    if (pipeline.ref == tag) {
      return Pipeline.fromGitlab(pipeline);
    }
  }

  return new Pipeline(
    "", 0, PipelineStatus.NotCreated
  );
}

export async function getContainerOfTag(
  projectId: number,
  tag: string,
): Promise<Container | null> {
  const gitlab = GitlabState.getInstance();
  const project = gitlab.getProject(projectId);

  const container = project.getContainer(tag);
  if (!container) {
    return null;
  }

  const aliases = project.getContainerAliases(container.shortRevision)
    .filter((cont) => cont.tag != container.tag)
    .map((cont) => cont.tag);

  return Container.fromGitlab(container, aliases);
}
