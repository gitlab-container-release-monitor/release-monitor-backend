import { config } from "./config"
import { Gitlab } from '@gitbeaker/node';

function timestamp(): number {
  return (new Date().getTime()) / 1000;
}

let cache = Object();

function asyncCached(ageSeconds: number, keySalt?: string) {

  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = async function(...args) {
      // cacheKey - a poor person's hash
      const cacheKey = propertyKey + (keySalt || "") + args.join("-");
      if (cache.hasOwnProperty(cacheKey)) {
        const [ts, value] = cache[cacheKey];
        if (timestamp() - ts < ageSeconds) {
          console.log("Cache hit for " + cacheKey);
          return value;
        }
      }
      console.log("Cache miss - run for key " + cacheKey)
      const result = await originalMethod.apply(this, args);
      cache[cacheKey] = [timestamp(), result];
      console.log("Cache store run for key " + cacheKey)
      return result;
    }

    return descriptor;
  };

}


export function tagToMajorString(tag: string): string | null {
  const pattern = /v?(?<major>[0-9]+).(?<minor>[0-9]+)\.[0-9]+.*/;
  const match = pattern.exec(tag);
  if (match === null) {
    return null;
  }
  const res = `${match.groups?.major}.${match.groups?.minor}`;
  console.log(res);
  return res;
}


export class CachedGitlab {
  gitlab: InstanceType<typeof Gitlab>;
  projectIds: Array<number>;

  constructor() {
    this.gitlab = new Gitlab({
      host: config.gitlabUrl,
      token: config.gitlabAccessToken,
    });
    this.projectIds = config.projects;
  }

  @asyncCached(300)
  async getProjectName(projectId: number): Promise<string> {
    const data = await this.gitlab.Projects.all(
      {
        "id_after": projectId - 1,
        "id_before": projectId + 1,
      }
    );
    return data[0]["name"];
  }

  @asyncCached(60)
  async getTagsOfProject(projectId: number): Promise<Array<string>> {
    let result = [];
    console.log("Fetch Tags of" + projectId);
    const data = await this.gitlab.Tags.all(projectId, {"perPage": 50});
    for (let entry of data) {
      result.push(entry["name"]);
    }
    return result;
  }

  @asyncCached(90)
  async getMajorReleases(): Promise<Array<string>> {
    let result = new Set<string>();
    console.log("Fetch Releases");
    for (let projectId of this.projectIds) {
      const data = await this.getTagsOfProject(projectId);
      for (let tag of data) {
        const major = tagToMajorString(tag);
        if (major !== null){
          result.add(major);
        }
      }
    }
    console.log(Array.from(result));
    return Array.from(result);
  }

  @asyncCached(30)
  async getNewestPipelineOfTag(
    projectId: number,
    ref: string,
  ): Promise<{
      url: string,
      timestampCreated: number,
      state: string,
    } | null> {
    console.log("Fetch Pipelines of project " + projectId);
    const data = await this.gitlab.Pipelines.all(projectId, {
      "scope": "tags",
      "ref": ref,
    });
    if (data.length == 0) {
      return null;
    }

    return {
      url: data[0]["web_url"],
      timestampCreated: new Date(data[0]["created_at"]).getTime() / 1000,
      state: data[0]["status"],
    };
  }

  async getTagsOfProjectOfRelease(projectId: number, majorRelease: string): Promise<Array<string>> {
    const allTags = await this.getTagsOfProject(projectId);
    return allTags.filter((x) => majorRelease === tagToMajorString(x));
  }

  @asyncCached(600)
  async getContainerRepositoryId(projectId: number): Promise<number | null> {
    const repositories = await this.gitlab.ContainerRegistry.projectRepositories(projectId);
    for (let entry of repositories) {
      if (entry.name == config.image) {
        return <number>entry.id;
      }
    }
    return null;
  }

  @asyncCached(60)
  async getContainerTags(projectId: number) {
    const repositoryId = await this.getContainerRepositoryId(projectId);
    const tags = await this.gitlab.ContainerRegistry.tags(projectId, repositoryId);
    return tags.map((x) => x.name);
  }

  @asyncCached(120)
  async getContainer(projectId: number, tagName: string): Promise<{
    location: string,
    shortRevision: string,
    createdAt: number,
    sizeBytes: number,
  } | null> {
    const repositoryId = await this.getContainerRepositoryId(projectId);
    let data;
    try {
      data = await this.gitlab.ContainerRegistry.showTag(projectId, repositoryId, tagName);
    } catch (err) {
      console.log("ERROR FETCHING CONTAINER");
      console.log(err);
      console.log("ERROR ABOVE ASSUMED TO BE 404");
      return null;
    }
    const created = new Date(data["created_at"]);
    return {
      location: data["location"],
      shortRevision: data["short_revision"],
      createdAt: created.getTime(),
      sizeBytes: data["total_size"],
    }
  }
}
