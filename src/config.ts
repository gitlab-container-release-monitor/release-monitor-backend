
import dotenv = require('dotenv');
dotenv.config();

export module config {
  export const port: number = parseInt(process.env.PORT || "3000");
  export const gitlabAccessToken = process.env.GITLAB_ACCESS_TOKEN;
  export const gitlabUrl = process.env.GITLAB_URL || "https://gitlab.com";
  export const projects: Array<number> = process.env.PROJECTS.split(',').map((x) => parseInt(x));
  export const image = process.env.RELEASE_IMAGE || "release";
  export const mount = process.env.MOUNT;

  if (!mount) {
    console.warn("Warning! MOUNT is not defined.");
    console.warn("Cannot register GitLab Webhooks");
  }
}
