
export class Tag {
  major: number;  // 2022
  minor: number;  // 3
  patch: number;  // 44
  original: string;  // v2022.3.44

  constructor(major: number, minor: number, patch: number, original?: string) {
    this.major = major;
    this.minor = minor;
    this.patch = patch;
    this.original = original || `${major}.${minor}.${patch}`;
  }

  equalTo(other: Tag): boolean {
    if (this.major !== other.major) {
      return false;
    }
    if (this.minor !== other.minor) {
      return false;
    }
    return (this.patch === other.patch);
  }

  isComplete(): boolean {
    return (this.major !== null) && (this.minor !== null) && (this.patch !== null);
  }

  greaterThan(other: Tag) {
    if (!this.isComplete() || !other.isComplete()) {
      throw new String("Cant compare releases without patch level");
    }
    if (this.major > other.major) {
      return true;
    }
    if (this.minor > other.minor) {
      return true;
    }
    return (this.patch > other.patch);
  }
}

export function parseTag(text: string): Tag | null {
  const pattern = /v?(?<major>[0-9]+).(?<minor>[0-9]+)\.(?<patch>[0-9]+).*/;
  const match = pattern.exec(text);
  if (match === null) {
    return null;
  }
  return new Tag(
    parseInt(match.groups?.major),
    parseInt(match.groups?.minor),
    parseInt(match.groups?.patch),
    text,
  );
}
