import { config } from "./config"
import { Gitlab } from '@gitbeaker/node';
import { Pipeline } from "./dto";
import { parseTag } from "./tagging";

const gitlab = new Gitlab({
  host: config.gitlabUrl,
  token: config.gitlabAccessToken,
});

function timestamp(): number {
  return (new Date().getTime()) / 1000;
}

export type GitlabContainer = {
  tag: string,
  location: string,
  shortRevision: string,
  createdAt: number,
  sizeBytes: number,
};

export type GitlabTag = {
  name: string,
  timestamp: number,
  message: string,
  commit: string,
  commitUrl: string,
};

export type GitlabPipeline = {
  url: string,
  ref: string,  // tag
  startedTimestamp: number,
  updatedTimestamp: number,
  status: string,
};

class ProjectState {
  id: number;
  name: string;
  description: string;
  url: string;
  avatarUrl: string | null;

  respositoryId: number | null;
  containers: Array<GitlabContainer>;
  tags: Array<GitlabTag>;
  pipelines: Array<GitlabPipeline>;

  constructor(projectId) {
    this.id = projectId;
  }

  async refreshMeta() {
    const data = await gitlab.Projects.show(this.id);
    this.name = data["name"];
    this.description = data["description"] || "";
    this.url = data["web_url"];
    this.avatarUrl = data["avatar_url"];
  }

  async refreshContainers() {
    const repositories = await gitlab.ContainerRegistry.projectRepositories(this.id); 
    loop: {
      for (let entry of repositories) {
        if (entry.name == config.image) {
          this.respositoryId = <number>entry.id;
          break loop;
        }
      }
      this.respositoryId = null;
      this.containers = [];
      return;
    }

    // [{name: 'v1.2.3', location: 'registry.gitlab.com/project-slug/release:v1.2.3'}]
    const tags = (
        await gitlab.ContainerRegistry
        .tags(this.id, this.respositoryId)
      )
      .filter((t) => parseTag(t.name) !== null);

    let newContainers = [];
    for (const tag of tags) {
      const containerData = await gitlab.ContainerRegistry.showTag(this.id, this.respositoryId, tag["name"]);
      newContainers.push({
        tag: tag["name"],
        location: tag["location"],
        shortRevision: containerData["short_revision"],
        createdAt: (new Date(containerData["created_at"])).getTime() / 1000,
        sizeBytes: containerData["total_size"],
      })
    }
    this.containers = newContainers;
  }
 
  async refreshTags() {
    // maximum of 100 Tags
    const gitTags = (
        await gitlab.Tags.all(this.id, {"perPage": 100})
      ).filter((t) => parseTag(t.name) !== null);

    let newTags = [];
    for (let tag of gitTags) {
      newTags.push(
        {
          name: tag["name"],
          timestamp: (new Date(tag["commit"]["created_at"])).getTime() / 1000,
          message: tag["message"],
          commit: tag["commit"]["short_id"],
          commitUrl: tag["commit"]["web_url"],
        }
      )
    }
    this.tags = newTags;
  }

  async refreshPipelines() {
    const pipelines = await gitlab.Pipelines.all(this.id, {
      "scope": "tags",
    });
    let newPipelines = [];
    for (let pipe of pipelines) {
      newPipelines.push({
        url: pipe["web_url"],
        ref: pipe["ref"],  // tag, as our scope is tags
        startedTimestamp: (new Date(pipe["created_at"])).getTime() / 1000,
        updatedTimestamp: (new Date(pipe["updated_at"])).getTime() / 1000,
        status: pipe["status"],
      })
    }
    this.pipelines = newPipelines;
  }

  async refreshHook() {
    if (!config.mount) {
      return;
    }
    const targetUrl = `${config.mount}/hook`;
    // Check if hook already exists
    const existingHooks = await gitlab.ProjectHooks.all(this.id);
    for (let hook of existingHooks) {
      if (hook.url == targetUrl) {
        return;
      }
    }
    // Register Hook
    await gitlab.ProjectHooks.add(this.id, targetUrl, {
      tag_push_events: true,
      pipeline_events: true,
    });
    console.log("Registered Webhook for Project:", this.name);
  }

  async refresh() {
    await this.refreshMeta();
    await this.refreshContainers();
    await this.refreshTags();
    await this.refreshPipelines();
    await this.refreshHook();
  }

  async hookRefreshPipeline(
    ref: string,
  ) {
    const data = await gitlab.Pipelines.all(this.id, {
      "scope": "tags",
      "ref": ref,
    });
    if (data.length == 0) {
      return;
    }
    const pipe = data[0];
    const newPipeline = {
      url: pipe["web_url"],
      ref: ref,  // tag, as our scope is tags
      startedTimestamp: (new Date(pipe["created_at"])).getTime() / 1000,
      updatedTimestamp: (new Date(pipe["updated_at"])).getTime() / 1000,
      status: pipe["status"],
    }
    for (let i in this.pipelines) {
      if (this.pipelines[i].ref == ref) {
        this.pipelines[i] = newPipeline;
        return;
      }
    }
    this.pipelines.push(newPipeline);
  }

  async hookPushTag(
    ref: string,
  ) {
    // ref/tags/v1.1.1 to v1.1.1
    const name = ref.split("/")[2];
    if (parseTag(name) === null) {
      console.log("Skip tag pushed:", name);
      return;
    }
    const data = await gitlab.Tags.show(this.id, name);
    const newTag = {
      name: name,
      timestamp: (new Date(data["commit"]["created_at"])).getTime() / 1000,
      message: data["message"],
      commit: data["commit"]["short_id"],
      commitUrl: data["commit"]["web_url"],
    };
    for (let i in this.tags) {
      if (this.tags[i].name == name) {
        this.tags[i] = newTag;
        return;
      }
    }
    this.tags.unshift(newTag);
  }

  getContainer(tag: string): GitlabContainer | null {
    for (let container of this.containers) {
      if (container.tag == tag) {
        return container;
      }
    }
  }

  getContainerAliases(shortRevision: string): Array<GitlabContainer> {
    return this.containers.filter((c) => c.shortRevision == shortRevision);
  }

  getTag(tagName: string): GitlabTag | null {
    for (let tag of this.tags) {
      if (tag.name == tagName) {
        return tag;
      }
    }
  }
}


export class GitlabState {
  private static instance: GitlabState;
  projects: Array<ProjectState>;

  private constructor() {
    this.projects = config.projects.map(
      (projectId) => new ProjectState(projectId)
    );
  }

  static getInstance() {
    if (!GitlabState.instance) {
      GitlabState.instance = new GitlabState();
    }
    return GitlabState.instance;
  }

  async refresh() {
    // TODO consider concurrency (batched)
    for (const project of this.projects) {
      await project.refresh();
    }
  }

  async processIncomingHook(data: any) {
    const projectId = data["project"]["id"];
    let projectState: ProjectState = this.getProject(projectId);
    if (!projectState) {
      console.log(`Unknown project ${projectId} of incoming hook. Ignoring.`);
      return;
    }
    console.log("Update project state:", projectState.name);

    // == Tag push ==
    if (data["object_kind"] == "tag_push") {
      await projectState.hookPushTag(data["ref"]);
      console.log("Updated project tag:", data["ref"], projectState.name);
    // == Pipeline update ==
    } else if (data["object_kind"] == "pipeline") {
      if (!data["object_attributes"]["tag"]) {
        console.log("Pipeline is not triggered by tag. Ignoring.", projectState.name)
        return;
      }
      await projectState.hookRefreshPipeline(
        data["object_attributes"]["ref"],
      );
      console.log("Pipeline updated", projectState.name);
      if (data["object_attributes"]["status"] == "success") {
        projectState.refreshContainers();
        console.log("Container updated", projectState.name);
      } 
    // == Fallback ==
    } else {
      console.log("Unknown incoming Hook format. Ignoring.", projectState.name);
      return;
    }
  }

  getProject(projectId: number): ProjectState | null {
    for (let projectState of this.projects) {
      if (projectState.id == projectId) {
        return projectState;
      }
    }
  }
}
