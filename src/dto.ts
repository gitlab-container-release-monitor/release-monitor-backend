import { GitlabContainer, GitlabTag, GitlabPipeline } from "./gitlabstate";


export enum ErrorType {
  BadRequest,
  NotFound,
  Unknown,
}

export class QueryResult<T> {
  ok: boolean;
  errorType: ErrorType;
  errorMessage: string | null;
  result: T | null;

  constructor(result: T | null, errorMessage?: string, type?: ErrorType) {
    this.ok = (result !== null);
    this.errorMessage = errorMessage;
    this.result = result;
    this.errorType = type || ErrorType.Unknown;
  }
}

export class Project {
  id: number;
  name: string;
  description: string;
  url: string;
  avatarUrl: string;

  constructor(
    id: number,
    name: string,
    description: string,
    url: string,
    avatarUrl: string,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.url = url;
    this.avatarUrl = avatarUrl;
  }
}

export class GitTag {
  name: string;
  timestamp: number;
  message: string;
  commit: string;
  commitUrl: string;

  constructor(
    name: string,
    timestamp: number,
    message: string,
    commit: string,
    commitUrl: string,
  ) {
    this.name = name;
    this.timestamp = timestamp;
    this.message = message;
    this.commit = commit;
    this.commitUrl = commitUrl;
  }

  static fromGitlab(gitlabTag: GitlabTag): GitTag {
    return new GitTag(
      gitlabTag.name,
      gitlabTag.timestamp,
      gitlabTag.message,
      gitlabTag.commit,
      gitlabTag.commitUrl,
    );
  }
}

export enum PipelineStatus {
  NotCreated = "not created",
  Pending = "pending",
  Running = "running",
  Failed = "failed",
  Successful = "successful",
  Unknown = "unknown",
}

function stringToPipelineStatus(t: string): PipelineStatus {
  // https://docs.gitlab.com/ee/api/pipelines.
  switch (t) {
    case "created":
    case "waiting_for_resource":
    case "preparing":
    case "pending":
      return PipelineStatus.Pending;
    case "running":
      return PipelineStatus.Running;
    case "success":
      return PipelineStatus.Successful;
    case "failed":
      return PipelineStatus.Failed;
    default:
      return PipelineStatus.Unknown;
  }
}

export class Pipeline {
  url: string;
  timestamp: number;
  status: PipelineStatus;

  constructor(url: string, timestamp: number, status: PipelineStatus) {
    this.url = url;
    this.timestamp = timestamp;
    this.status = status;
  }

  static fromGitlab(gitlabPipeline: GitlabPipeline): Pipeline {
    return new Pipeline(
      gitlabPipeline.url,
      gitlabPipeline.updatedTimestamp,
      stringToPipelineStatus(gitlabPipeline.status),
    )
  }
}

export class Container {
  tag: string;
  timestamp: number;
  bytes: number;
  shortDigest: string;
  aliases: Array<string>;
  pullString: string;  // docker pull-able

  constructor(
    tag: string,
    timestamp: number,
    bytes: number,
    shortDigest: string,
    pullString: string,
    aliases?: Array<string>,
  ) {
    this.tag = tag;
    this.timestamp = timestamp;
    this.bytes = bytes;
    this.shortDigest = shortDigest;
    this.pullString = pullString;
    this.aliases = aliases || [];
  }

  static fromGitlab(
    gitlabContainer: GitlabContainer,
    aliases: Array<string>,
  ): Container {
    return new Container(
      gitlabContainer.tag,
      gitlabContainer.createdAt,
      gitlabContainer.sizeBytes,
      gitlabContainer.shortRevision,
      gitlabContainer.location,
      aliases,
    );
  }
}
