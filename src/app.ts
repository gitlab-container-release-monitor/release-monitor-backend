import {
  getReleases,
  getProjects,
  getTagsOfProjectOfRelease,
  getLatestOfProjectOfRelease,
  getPipelineOfTag,
  getContainerOfTag,
} from "./domain"
import { QueryResult, ErrorType } from "./dto"
import { config } from "./config"
import { GitlabState } from "./gitlabstate"
import express = require('express')

const app = express()
app.use(express.json());  // allow to read post body as json

const port = config.port;

app.post('/hook', async (req, res) => {
  console.log("POST /hook");
  const body = req.body;
  await GitlabState.getInstance().processIncomingHook(body);
  res.send("OK");
});

app.get('/releases', async (req, res) => {
  console.log("GET /releases")
  res.send(await getReleases());
});

app.get('/projects', async (req, res) => {
  res.send(await getProjects());
})

app.get('/project/:projectId/release/:release/tags', async (req, res) => {
  res.send(await getTagsOfProjectOfRelease(
    parseInt(req.params.projectId),
    req.params.release,
  ));
});

app.get('/project/:projectId/release/:release/latest', async (req, res) => {
  res.send(await getLatestOfProjectOfRelease(
    parseInt(req.params.projectId),
    req.params.release,
  ));
})

app.get('/project/:projectId/tag/:tag/pipeline', async (req, res) => {
  res.send(await getPipelineOfTag(
    parseInt(req.params.projectId),
    req.params.tag,
  ))
})

app.get('/project/:projectId/tag/:tag/container', async (req, res) => {
  const result = await getContainerOfTag(
    parseInt(req.params.projectId),
    req.params.tag,
  );
  if (result === null) {
    res.send(new QueryResult(null, "No container found", ErrorType.NotFound));
    return;
  }
  res.send(result);
});

async function refreshRegulary() {
  while (true) {
    // Sleep 900s
    await new Promise(r => setTimeout(r, 900_000));
    console.log("Refresh internal state");
    await GitlabState.getInstance().refresh();
  }
}

app.listen(port, async () => {
  console.log("Initialize internal state")
  await GitlabState.getInstance().refresh();
  console.log("Internal state generated");
  // spawn background async thread
  setTimeout(refreshRegulary, 0);
  console.log(`Example app listening on port ${port}`)
});

